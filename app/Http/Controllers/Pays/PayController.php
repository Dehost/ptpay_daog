<?php

namespace App\Http\Controllers\Pays;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayController extends Controller
{
		private $login;
		private $tranKey;
		protected $seed;
		protected $additional;

		public function __construct()
		{
				$this->login = '6dd490faf9cb87a9862245da41170ff2';
				$this->tranKey = '024h1llD';
				$this->seed = date('c');
				$this->additional = [];
		}

    public function bankList(Request $request)
    {
    		// if( $request->ajax() )
    		// {
    		// 
    		
						try
						{
						    $opts = array(
						        'http' => array(
						            'user_agent' => 'PHPSoapClient'
						        ),
						    );
						    $context = stream_context_create($opts);
						    $wsdlUrl = 'https://test.placetopay.com/soap/pse/?wsdl';

			        	$paramKey = [ 
			        			'login'=> $this->login,
			        			'tranKey'=> sha1($this->seed.$this->tranKey,false),
			        			'seed'=> $this->seed,
			        			'additional'=> $this->additional
			        	];

						    $soapClientOptions = array(
						        'stream_context' => $context,
                    'trace'=> true, "exceptions"=>true,
                    "style" => SOAP_DOCUMENT, "use" => SOAP_LITERAL, 
                    "encoding" => 'utf-8',"soap_version" => SOAP_1_1,
                    "connection_timeout"=>2560,
						    );

						    $client = new \SoapClient($wsdlUrl, $soapClientOptions);

						    //dd($client->__getTypes());

						    dd( $client->getBankList(['auth'=> $paramKey]) );
						   	

		    				//echo "</pre>";
						}
						catch(Exception $e) 
						{
						    echo $e->getMessage();
						}
						    		
    		//}
    }

}
