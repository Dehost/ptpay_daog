<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->string('document_type',3);
            $table->string('document', 12);
            $table->string('first_name', 60);
            $table->string('last_name', 60);
            $table->string('company', 60);
            $table->string('email_address', 100);
            $table->string('address', 80);
            $table->string('city', 50);
            $table->string('province', 50);
            $table->string('country', 2);
            $table->string('phone', 30);
            $table->string('mobile', 30);
            $table->timestamps();

            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
